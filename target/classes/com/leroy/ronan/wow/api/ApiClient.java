package com.leroy.ronan.wow.api;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.leroy.ronan.wow.api.services.ApiClientCacheService;
import com.leroy.ronan.wow.api.services.ApiClientFileService;
import com.leroy.ronan.wow.api.services.ApiClientService;
import com.leroy.ronan.wow.api.services.ApiClientWebService;
import com.leroy.ronan.wow.beans.WowCharacter;
import com.leroy.ronan.wow.beans.WowGuild;

public class ApiClient {

	private String zone;
	private ApiClientService web;
	private ApiClientService file;
	private ApiClientService mem;
	
	private int webCallCount;
	
	public ApiClient(String zone) {
		this.zone = zone;
		
		this.webCallCount = 0;
        this.web = new ApiClientWebService();
        this.file = new ApiClientFileService("apifiles", LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT), Duration.ofHours(24));
        this.mem = new ApiClientCacheService();
	}
	
	public WowCharacter getCharacter(String realm, String name) {
        ApiResponse data = this.getData(ApiType.character, realm, name);
        return new WowCharacter(data.getJSON());
	}
	
	public WowGuild getGuild(String realm, String name) {
		ApiResponse data = this.getData(ApiType.guild, realm, name);
        return new WowGuild(data.getJSON());
	}

	private ApiResponse getData(ApiType type, String realm, String name) {
		ApiResponse res = mem.getData(zone, type, realm, name);
        if (res == null){
            res = file.getData(zone, type, realm, name);
            if (res == null){
            	this.webCallCount++;
                res = web.getData(zone, type, realm, name);
                file.putData(zone, type, realm, name, res.getJSON());
            }
            mem.putData(zone, type, realm, name, res.getJSON());
        }
        return res;
	}
	
	public int getApiCallCount() {
		return this.webCallCount;
	}
	
	public boolean isCharacterPersisted(String realm, String name) {
		boolean res = false;
		if (this.getData(ApiType.character, realm, name) != null){
			res = true;
		}
		return res;
	}
	
	public void setCharacterData(String realm, String name, String data) {
        file.putData(zone, ApiType.character, realm, name, data);
	}

}
