package com.leroy.ronan.wow.api.services;

import com.leroy.ronan.wow.api.ApiResponse;
import com.leroy.ronan.wow.api.ApiType;

public interface ApiClientService {

	public ApiResponse getData(String zone, ApiType type, String realm, String name);

	public void putData(String zone, ApiType type, String realm, String name, String json);

}
