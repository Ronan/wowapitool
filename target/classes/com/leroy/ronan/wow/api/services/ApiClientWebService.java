package com.leroy.ronan.wow.api.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.leroy.ronan.wow.api.ApiResponse;
import com.leroy.ronan.wow.api.ApiType;

public class ApiClientWebService implements ApiClientService{

    private final static Logger logger = Logger.getLogger(ApiClientWebService.class);

	public ApiResponse getData(String zone, ApiType type, String realm, String name) {
        String path = "/wow/"+type+"/"+realm+"/"+name;
        List<String> optionsList = new ArrayList<>();
        optionsList.add("locale=en_GB");
        optionsList.add("apikey=8vkxyhwqkb787e47utga6r5djuw2unqt");
        if (type.getFields() != null && type.getFields().size() > 0){
            optionsList.addAll(type.getFields().stream().map(s -> "fields=" + s).collect(Collectors.toList()));
        }
        String options = String.join("&", optionsList); 
        String data = null;
		try {
			URI uri = new URI("https", zone+".api.battle.net", path, options, null);
			URL url = uri.toURL();
			URLConnection conn = url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			data = "";
			String inputLine;
			while ((inputLine = br.readLine()) != null) {
			    data += inputLine;
			}
			br.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
        return new ApiResponse(data);
	}

	public void putData(String zone, ApiType type, String realm, String name, String json) {
		// Do nothing.
		logger.error("Cannot put data with this service!");
	}

}
