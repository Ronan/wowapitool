package com.leroy.ronan.wow.api;

import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Assert;

import com.leroy.ronan.wow.beans.WowCharacter;
import com.leroy.ronan.wow.beans.WowGuild;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ApiSteps {
	
	private ApiClient client;
	private String realm;
	private String characterName;
	private String guildName;
	
	private WowCharacter character;
	private WowGuild guild;
	
	@Given("^a battlenet api client on zone \"(.*?)\"$")
	public void a_battlenet_client_on_zone(String zone) throws Throwable {
		this.client = new ApiClient(zone);
	}

	@Given("^realm name is \"(.*?)\"$")
	public void realm_name_is(String realm) throws Throwable {
		this.realm = realm;
	}

	@Given("^character name is \"(.*?)\"$")
	public void character_name_is(String name) throws Throwable {
		this.characterName = name;
	}
	
	@Given("^character data does not exist in the file system$")
	public void character_data_does_not_exist_in_the_file_system() throws Throwable {
		this.client.setCharacterData(this.realm, this.characterName, null);
	}
	
	@Given("^character data exists in the file system$")
	public void character_data_exists_in_the_file_system() throws Throwable {
		String data = new String(Files.readAllBytes(Paths.get(getClass().getResource("sample.json").toURI())));
		this.client.setCharacterData(this.realm, this.characterName, data);
	}
	
	@Given("^guild name is \"(.*?)\"$")
	public void guild_name_is(String guild) throws Throwable {
	    this.guildName = guild;
	}

	@When("^I get the character data$")
	public void i_get_the_character_data() throws Throwable {
		this.client.getCharacter(this.realm, this.characterName);
	}
	
	@When("^I get the member list$")
	public void i_get_the_member_list() throws Throwable {
		this.guild = this.client.getGuild(this.realm, this.guildName);
	}

	@Then("^the API was only called once$")
	public void the_API_was_only_called_once() throws Throwable {
		Assert.assertEquals(1, this.client.getApiCallCount());
	}

	@Then("^data for the character has been saved in the file system$")
	public void data_for_the_character_has_been_saved_in_the_file_system() throws Throwable {
		Assert.assertTrue(this.client.isCharacterPersisted(this.realm, this.characterName));
	}

	@Then("^the API was never called$")
	public void the_API_was_never_called() throws Throwable {
		Assert.assertEquals(0, this.client.getApiCallCount());
	}
	
	@Then("^a character with name \"(.*?)\" is in the list$")
	public void a_character_with_name_is_in_the_list(String name) throws Throwable {
		Assert.assertEquals(1, this.guild.getMembers().stream().filter(m -> m.getName().equals(name)).count());
	}

	@Then("^I am able to know the ilvl of this character$")
	public void i_am_able_to_know_the_ilvl_of_this_character() throws Throwable {
		WowCharacter character = this.client.getCharacter(this.realm, this.characterName);
		Assert.assertNotNull(character.getAverageItemLevel());
	}

	@Then("^I am able to know the achievementPoints of this character$")
	public void i_am_able_to_know_the_achievementPoints_of_this_character() throws Throwable {
		WowCharacter character = this.client.getCharacter(this.realm, this.characterName);
		Assert.assertNotNull(character.getAchievementPoints());
	}

	@Then("^I am able to know this character is a reroll of \"(.*?)\" in realm \"(.*?)\"$")
	public void i_am_able_to_know_this_character_is_a_reroll_of(String otherName, String otherRealm) throws Throwable {
		WowCharacter character = this.client.getCharacter(this.realm, this.characterName);
		WowCharacter otherCharacter = this.client.getCharacter(otherRealm, otherName);
		Assert.assertEquals(character.getAchievementPoints(), otherCharacter.getAchievementPoints());
	}
}
