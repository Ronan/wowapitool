package com.leroy.ronan.wow.api;

import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Assert;

import com.leroy.ronan.wow.beans.WowCharacter;
import com.leroy.ronan.wow.beans.WowGuild;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class FileSystemCacheSteps {
	
	private World world;

	public FileSystemCacheSteps(World world) {
		this.world = world;
	}

	@Given("^character data does not exist in the file system$")
	public void character_data_does_not_exist_in_the_file_system() throws Throwable {
		world.getClient().setCharacterData(world.getRealm(), world.getCharacterName(), null);
	}
	
	@Given("^character data exists in the file system$")
	public void character_data_exists_in_the_file_system() throws Throwable {
		String data = new String(Files.readAllBytes(Paths.get(getClass().getResource("sample.json").toURI())));
		world.getClient().setCharacterData(world.getRealm(), world.getCharacterName(), data);
	}
	
	@Then("^data for the character has been saved in the file system$")
	public void data_for_the_character_has_been_saved_in_the_file_system() throws Throwable {
		Assert.assertTrue(world.getClient().isCharacterPersisted(world.getRealm(), world.getCharacterName()));
	}

	@Then("^the API was never called$")
	public void the_API_was_never_called() throws Throwable {
		Assert.assertEquals(0, world.getClient().getRemoteCallCount());
	}
	
}
