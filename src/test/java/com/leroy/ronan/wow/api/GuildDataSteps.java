package com.leroy.ronan.wow.api;

import org.junit.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GuildDataSteps {

	private World world;

	public GuildDataSteps(World world) {
		this.world = world;
	}

	@Given("^guild name is \"(.*?)\"$")
	public void guild_name_is(String guild) throws Throwable {
	    world.setGuildName(guild);
	}

	@When("^I get the member list$")
	public void i_get_the_member_list() throws Throwable {
		world.setGuild(world.getClient().getGuild(world.getRealm(), world.getGuildName()));
	}

	@Then("^a character with name \"(.*?)\" is in the list$")
	public void a_character_with_name_is_in_the_list(String name) throws Throwable {
		Assert.assertEquals(1, world.getGuild().getMembers().stream().filter(m -> m.getName().equals(name)).count());
	}

}
