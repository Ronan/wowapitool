package com.leroy.ronan.wow.api;

import com.leroy.ronan.wow.beans.WowCharacter;
import com.leroy.ronan.wow.beans.WowGuild;

public class World {

	private ApiClient client;
	private String realm;
	private String guildName;
	private String characterName;
	
	private WowGuild guild;
	private WowCharacter character;
	
	public ApiClient getClient() {
		return client;
	}
	public void setClient(ApiClient client) {
		this.client = client;
	}
	public String getRealm() {
		return realm;
	}
	public void setRealm(String realm) {
		this.realm = realm;
	}
	public String getGuildName() {
		return guildName;
	}
	public void setGuildName(String guildName) {
		this.guildName = guildName;
	}
	public String getCharacterName() {
		return characterName;
	}
	public void setCharacterName(String characterName) {
		this.characterName = characterName;
	}
	public WowGuild getGuild() {
		return guild;
	}
	public void setGuild(WowGuild guild) {
		this.guild = guild;
	}
	public WowCharacter getCharacter() {
		return character;
	}
	public void setCharacter(WowCharacter character) {
		this.character = character;
	}
	

	
}
