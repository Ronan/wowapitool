package com.leroy.ronan.wow.api;

import org.junit.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class ApiSteps {

	private World world;

	public ApiSteps(World world) {
		this.world = world;
	}

	@Given("^a battlenet api client on zone \"(.*?)\"$")
	public void a_battlenet_client_on_zone(String zone) throws Throwable {
		world.setClient(new ApiClient(zone));
	}

	@Then("^the API was only called once$")
	public void the_API_was_only_called_once() throws Throwable {
		Assert.assertEquals(1, world.getClient().getRemoteCallCount());
	}

}
