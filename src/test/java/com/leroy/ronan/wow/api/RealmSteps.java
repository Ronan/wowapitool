package com.leroy.ronan.wow.api;

import cucumber.api.java.en.Given;

public class RealmSteps {

	private World world;

	public RealmSteps(World world) {
		this.world = world;
	}

	@Given("^realm name is \"(.*?)\"$")
	public void realm_name_is(String realm) throws Throwable {
		world.setRealm(realm);
	}


}
