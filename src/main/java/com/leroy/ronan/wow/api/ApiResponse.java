package com.leroy.ronan.wow.api;

public class ApiResponse {

    private String json;

	public ApiResponse(String json) {
        super();
        this.json = json;
	}

	public String getJSON() {
		return json;
	}

}
